//
// Created by lore on 4/13/19.
//

#include "Dataset.h"

bool Dataset::isPunctuation(char ch) {
    switch(ch) {
        case ' ': { return true; }
        case ',': { return true; }
        default: { return false; }
    }
}

std::vector<std::string> *Dataset::splitColNames(std::string line, int nCols, char sep) {
    // Splits the words in a string separated by 'sep' and return a vector
    int jj = 0;
    std::string buff;
    auto *res = new std::vector<std::string>;
    for (auto &ch:line) {
        if (isPunctuation(ch)) {
            if (ch == sep) {
                if (jj < nCols) {
                    res->push_back(buff);
                    buff.clear();
                    ++jj;
                }
            }
        } else {
            buff += ch;
        }
    }
    if (jj < nCols) { res->push_back(buff); }
    buff.clear();
    return res;
}

Dataset::Dataset(const char *filename, int nRows, int nCols, bool header, char sep) {

    std::string line;
    std::ifstream ifs;
    m_nRows = nRows;
    m_nCols = nCols;
    ifs.open(filename);

    try {
        if (ifs.fail())
            throw "File cannot be opened.";

        if (header) {
            getline(ifs, line);
            m_colNames = splitColNames(line, m_nCols);
        }

        if (m_values != nullptr)
            throw "Attribute m_values is not set to nullptr.";

        m_values = new float[m_nRows * m_nCols];
        std::string buff;
        int ii{0};
        int jj{0};

        while (getline(ifs, line)) {
            jj = 0;
            for (auto &ch:line) {
                if (isPunctuation(ch)) {
                    if (ch == sep) {
                        if (jj < m_nCols) {
                            m_values[ii * m_nCols + jj] = std::stof(buff);
                            buff.clear();
                            ++jj;
                        }
                    }
                } else {
                    buff += ch;
                }
            }
            if (jj < m_nCols) { m_values[ii * m_nCols + jj] = std::stof(buff); }
            buff.clear();
            ++ii;
        }
    } catch (const char *err) {
        std::cerr << err << '\n';
        ifs.close();
        delete[] m_values;
        delete m_colNames;
    }
    ifs.close();
}

Dataset::Dataset(float *values, int nRows, int nCols):
        m_values{values}, m_nRows{nRows}, m_nCols{nCols} { }

Dataset::Dataset(float *values, int nRows,  int nCols, std::vector<std::string> *colNames):
    Dataset(values, nRows, nCols) { m_colNames = colNames; }

void Dataset::print() {
    for (int i = 0; i < m_nRows; ++i) {
        for (int j = 0; j < m_nCols; ++j) {
            std::cout << m_values[i * m_nCols + j] << ' ';
        }
        std::cout << '\n';
    }
}

void Dataset::head(int n) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m_nCols; ++j) {
            std::cout << m_values[i * m_nCols + j] << ' ';
        }
        std::cout << '\n';
    }
}

void Dataset::tail(int n) {
    for (int i = m_nRows - n; i < m_nRows; ++i) {
        for (int j = 0; j < m_nCols; ++j) {
            std::cout << m_values[i * m_nCols + j] << ' ';
        }
        std::cout << '\n';
    }
}

std::ostream &operator<<(std::ostream &out, Dataset *dataset) {
    dataset->print();

    return out;
}

void Dataset::writeToCSV(const char *filename, bool header, char sep) {
    std::ofstream ofs;
    ofs.open(filename);

    if (header) {
        assert(m_colNames != nullptr);
        ofs << m_colNames->at(0);
        for (int i = 1; i < m_colNames->size(); ++i) {
            ofs << sep << m_colNames->at(i);
        }
        ofs << '\n';
    }

    for (int i = 0; i < m_nRows - 1; ++i) {
        ofs << m_values[i * m_nCols];
        for (int j = 1; j < m_nCols; ++j) {
            ofs << sep << m_values[i * m_nCols + j];
        }
        ofs << '\n';
    }

    ofs << m_values[(m_nRows - 1) * m_nCols];
    for (int j = 1; j < m_nCols; ++j)
        ofs << sep << m_values[(m_nRows - 1) * m_nCols + j];

    ofs.close();
}

Dataset *Dataset::subset(int n) {
    auto *subset_values = new float[n * m_nCols];
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m_nCols; ++j) {
            subset_values[i * m_nCols + j] = m_values[i * m_nCols + j];
        }
    }
    return new Dataset(subset_values, n, m_nCols, m_colNames);
}
